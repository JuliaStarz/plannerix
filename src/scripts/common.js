import 'slick-carousel';
import Popup from './popup';
import Inputmask from "inputmask";

$(function () {
    var popup = new Popup('#popup');

    $('body').on('click', 'a[href*="#"]', function(e) {
        e.preventDefault();
        if($(this).hasClass('popup-btn')) {
            popup.activate(this.hash);
        } else
            $('html,body').animate({scrollTop:$(this.hash).offset().top - 100}, 500);
    });

    $('.header-nav-btn').on('click', function(){
        $('.nav').slideToggle();
        $('.header').toggleClass('visible-nav');
    });

    /** form validation */
    window.addEventListener('load', function() {
        $('.needs-validation').on('submit', function(e){
            e.preventDefault();
            var form = $(this);
            var errors = [];

            var emailTest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            $(this).find('input, select, textarea').each(function() {
                $(this).removeClass('error');
                if(this.required && 
                    ((this.type == 'checkbox' && !this.checked) || 
                    this.value.trim() == '' ||
                    ($(this).hasClass('phone-mask') && this.value.replace(/[)+(_-]/g, '').length < 11) ||
                    (this.type == 'email' && !emailTest.test((this.value).toLowerCase())))
                ) {
                    errors.push(this.name);
                    $(this).addClass('error');
                }
            });

            form.addClass('was-validated');

            if(!errors.length) {
                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: form.serialize(),
        
                    complete: function(data, status) {
                        form[0].reset();
                        popup.activate('#thanks');
                        form.removeClass('was-validated');
                    }
                });
            }
        });
    }, false);

    var im = new Inputmask("+7(999)999-99-99");
    im.mask('.phone-mask');

    function isScrolledIntoView(elem, docViewTop) {
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var coef = ($(window).height() < 768) ? 4 : 2;
        var elemBottom = elemTop + ($(elem).height() / coef);

        return (elemBottom <= docViewBottom); // && (elemTop >= docViewTop)
    }

    $(window).scroll(function(){
        var offsetTop = $(this).scrollTop();

        if(offsetTop > 0) {
            $('#header').addClass('header-reverse')
        } else {
            $('#header').removeClass('header-reverse');
        }

        if(isScrolledIntoView('#footer_form', offsetTop)) {
            $('#footer_form').addClass('isVisible');
        } else {
            $('#footer_form').removeClass('isVisible');
        }

        if(isScrolledIntoView('#download', offsetTop)) {
            $('.download-images_list-item').addClass('isVisible');
        } else {
            $('.download-images_list-item').removeClass('isVisible');
        }

        if(isScrolledIntoView('#assistant', offsetTop)) {
            $('.assistant-preview').addClass('isVisible');
        } else {
            $('.assistant-preview').removeClass('isVisible');
        }


        if(isScrolledIntoView('#constructor', offsetTop)) {
            $('.constructor-li').first().addClass('active current');
        } else {
            $('.constructor-li').removeClass('active current');
        }

        $('.nav-link').each(function(index, el){
            if(isScrolledIntoView($(el).attr('href'), offsetTop)) {
                $(el).addClass('active');
                $(el).parent('li').siblings().find('a').removeClass('active');
            }
        })
    });

    $('.constructor-container').scroll(function(){
        var winW = $(window).width();
        var sliderW = $(this).width();

        if(winW < 1200) {
            $('.constructor-li').each(function(index){
                $(this).removeClass('active current');
                if(($(this).offset().left + 120) < sliderW) {
                    $(this).addClass('active current');
                    //$(this).siblings().removeClass('current');
                }
            });
        }
    })

    $('.constructor-li').on('mouseover click', function() {
        $(this).prevAll().addClass('active current');
        $(this).nextAll().removeClass('active current');
        $(this).addClass('current active');
        //$(this).siblings().removeClass('current');
    }); 

    /** sliders */
    $('.stages-slider').on('init', function(event, slick){
        var el = $(this).parents('.content').find('.stages-type:nth-child(1)');
        $(el).addClass('current');
        $(el).siblings().removeClass('current');
    });

    $('.portfolio-list').on('init', function(event, slick){
        var el = $(this).parents('.content').find('.type-item:nth-child(1)');
        
        activateTab(el);
    });

    $(".benefits-slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            }, 
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.stages-slider').slick({
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        infinite: false,
        adaptiveHeight: true
    });

    $('.portfolio-list').slick({
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        arrows: false
    });
    
    $('.portfolio-list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $(this).parents('.content').find('.type-item:nth-child(' + (nextSlide + 1) + ')').click();
    });
    $('.stages-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $(this).parents('.content').find('.stages-type:nth-child(' + (nextSlide + 1) + ')').click();
    });
          
    $(".orders-slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 1,
                    variableWidth: true,
                    centerMode: true
                }
            }, 
            {
                breakpoint: 567,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    /* activate the slide */
    function activateSlide(slider, index) {
        if($('.' + slider).slick('slickCurrentSlide') !== index)
            $('.' + slider).slick('slickGoTo', index);
    }

    function activateDiv(slider, index) {
        $('.' + slider).children().eq(index).show();
        $('.' + slider).children().eq(index).siblings().hide();
        $('.partners-list_item').each(function(index, el){
            if($(this).hasClass('slick-initialized'))
                $(this).slick('unslick');
        });
        $('.partners-list_item').eq(index).slick({
            slidesToShow: 4,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    }

    function activateDesc(slider, index) {
        $('.' + slider).children().eq(index).show();
        $('.' + slider).children().eq(index).siblings().hide();
    }

    function activateTab(el) {
        el.addClass('current');
        el.siblings().removeClass('current');

        var indicator = el.parents('.type-container').find('.type-indicator'),
            indicatorLeft = indicator.position().left;

        var leftOffset = el.find('span').position().left,
            elWidth = el.find('span').width(),
            parentWidth = el.parents('.type-container').outerWidth(),
            rightOffset = parentWidth - elWidth - leftOffset;

        var scrolledLeft = el.parents('.type-container').scrollLeft();

        if(indicatorLeft < leftOffset)
            indicator.removeClass('swipe-right').addClass('swipe-left');
        else
            indicator.removeClass('swipe-left').addClass('swipe-right');

        indicator.animate({
            left: leftOffset + scrolledLeft,
            right: rightOffset - scrolledLeft
        })
    }

    $('.type-list').on('click', '.type-item', function(){
        var el = $(this),
            index = el.index(),
            slider = el.parent().data('slider');

        if(el.hasClass('partners_type-list-item'))
            activateDiv(slider, index);
        else if(el.hasClass('services_type-list-item'))
            activateDesc(slider, index);
        else
            activateSlide(slider, index);
        
        activateTab(el);
    });

    $('.stages-types').on('click', '.stages-type', function () {
        var index = $(this).index();

        activateSlide('stages-slider', index);

        $(this).addClass('current');
        $(this).siblings().removeClass('current');
    });

    $('.partners_type-list-item').first().click();
    $('.services_type-list-item').first().click();

    $(window).resize(function() {
        $('.type-item.current').each(function(index, el){
            activateTab($(el));
        })
    });

    /** parallax */
    /*$("#first-screen img").parallax({
        mouseport: '#first-screen',
        yparallax: .1,
        xparallax: .1,
        decay: .1
    });
    $("#footer .parallax-layer").parallax({
        mouseport: '#footer',
        yparallax: .2,
        xparallax: .1
    });
    $('.screen').each(function(index, el){
        $(this).find(".parallax-layer").parallax({
            mouseport: el,
            yparallax: .4,
            xparallax: .3
        });
        $(this).find(".parallax-layer-vertical").parallax({
            mouseport: el,
            yparallax: .4,
            xparallax: false
        });
    });*/

    var lFollowX = 0,
        lFollowY = 0;

    var positions = {};

    function moveBackground(screen) {
        $(screen).find('.parallax-layer').each(function(index) {
            var position = positions[index] 
                            ?   positions[index]
                            :   {x: 0, y: 0, friction: (Math.random() + .1)}; 
            positions[index] = position;

            var x = position.x;
            var y = position.y;
            position.x += (lFollowX - position.x) * position.friction;
            position.y += (lFollowY - position.y) * position.friction;
            
            var translate = 'translate(' + position.x + 'px, ' + position.y + 'px)';
            
            $(this).css({
                '-webkit-transform': translate,
                '-moz-transform': translate,
                'transform': translate
            });
        });
    }

    $('.screen').on('mousemove', function(e) {
        var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
        var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
        lFollowX = (2 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
        lFollowY = (4 * lMouseY) / 100;
        
        moveBackground(this);
    });

    /*$('.type-container').each(function(e) {
        
    });
    var myElement = document.getElementById('first-screen');

    var mc = new Hammer.Manager(myElement);
    mc.add(new Hammer.Pan());
    mc.on("panmove", Hammer.bindFn(function() {
        console.log(this);
    }, this));

    $('.type-container, .constructor-container').on('mousemove', function(e) {
        var scrollLeft = $(this).scrollLeft();

        console.log(scrollLeft);
    });*/
});