class Popup {
    constructor(name){
        this.name = name;
        var self = this;

        $(name).on('click', () => {
            this.close();
        });
        $(name).on('click', '.popup-container', (e) => {
            e.stopPropagation();
        });
        $(name).on('click', '.popup-close', (e) => {
            this.close();
        });
        $(name).on('click', '.step-next', function(e) {
            var slide = $(this).data('slide');
            self.testNext(slide);
        });
        $(name).on('click', '.step-prev', function(e) {
            var slide = $(this).data('slide');
            self.testPrev(slide);
        });
    }
    activate(id){
        $(id).show();
        $(id).siblings().hide();
        this.show();
    }
    close(){
        $(this.name).removeClass('visible');
        $('body').removeClass('popup-active');
    }
    show() {
        $(this.name).addClass('visible');
        $('body').addClass('popup-active');
    }
    testNext(slide) {
        var ind = slide + 1;
        $(this.name).find('.step-' + ind).siblings().hide();
        $(this.name).find('.step-' + ind).show();
    }
    testPrev(slide) {
        var ind = slide - 1;
        $(this.name).find('.step-' + ind).siblings().hide();
        $(this.name).find('.step-' + ind).show();
    }
}

export default Popup;