const server = require('express');
const bodyParser = require('body-parser');
const mailer = require('./mailer.js');

const app = server();

const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(server.static(__dirname + '/static'));

app.post('/info', (req, res) => {
    mailer.sendInfo(JSON.stringify(req.body));
    res.sendStatus(200);
});

app.listen(port, () => console.log('Listening port ' + port));