const mailer = require('nodemailer');
const mailConfig = require('./mail.config.js');

const MAIL_INFO_SUBJECT = 'Информационное письмо с сайта';

const transporter = mailer.createTransport(mailConfig);

module.exports = {
    sendInfo: function(data) {
        transporter.sendMail({
            from: mailConfig.auth.user,
            to: '', // почта, куда отправляются письма
            text: data,
            subject: MAIL_INFO_SUBJECT + "[" + JSON.parse(data).tag + "]"
        })
    }
};